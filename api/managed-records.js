import fetch from "../util/fetch-fill";
import URI from "urijs";

// /records endpoint
window.path = "http://localhost:3000/records";

// get open records and add isPrimary boolean key
function getOpen(records) {

  let open = records.filter(record => record.disposition === 'open')
  return open.filter(record => {
    ['red', 'blue', 'yellow'].includes(record.color) ? record.isPrimary = true : record.isPrimary = false
    return record
  })

}

// get closed records with primary color count
function getClosed(records) {

  let closed = records.filter(record => record.disposition != 'open')
  return closed.filter(record => ['red', 'blue', 'yellow'].includes(record.color)).length

}

/* Retrieve function
------------------------------*/
function retrieve(options) {

  let recsPerPage = 10,
    colors = options && options.colors ? options.colors : [],
    page = options && options.page ? options.page : 1,
    hasNextPage,
    url = new URI(window.path).addSearch({
      limit: recsPerPage,
      offset: page > 1 ? (page * recsPerPage) - recsPerPage : '',
      "color[]": colors
    })

  return fetch(url).then(response => {
    if (!response.ok) {
      console.log(response.statusText)
    }
    return response.json()
  }).then(records => {

    url.removeSearch('offset').addSearch({
      offset: ((page + 1) * recsPerPage) - recsPerPage
    })
    return fetch(url).then(response => {
      return response.json()
    }).then(response => {

      response.length ? hasNextPage = true : hasNextPage = false
      return records

    })

  }).then(records => {

    let openrecords = getOpen(records),
      closedrecords = getClosed(records),
      recordsObj = {
        ids: records.map(record => record.id),
        open: openrecords,
        closedPrimaryCount: closedrecords,
        previousPage: page >= 2 ? page - 1 : null,
        nextPage: !records.length || !hasNextPage ? null : page + 1
      }

    return recordsObj

  }).catch(error => {
    console.log('fetch error: ', error)
  })
}

export default retrieve;